package com.nlmk.evteev.tm.exceptions;

public class ProjectNotFoundException extends Exception {

    /**
     * Ошибка при поиске проекта
     *
     * @param errorMsg строка сообщения
     */
    public ProjectNotFoundException(String errorMsg) {
        super(errorMsg);
    }

}
