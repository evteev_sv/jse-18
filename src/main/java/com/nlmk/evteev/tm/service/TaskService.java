package com.nlmk.evteev.tm.service;

import com.nlmk.evteev.tm.entity.Task;
import com.nlmk.evteev.tm.exceptions.TaskNotFoundException;
import com.nlmk.evteev.tm.repository.TaskRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collections;
import java.util.List;

/**
 * Сервисный класс для работы с репозиторием задач
 */
public class TaskService {

    private final Logger logger = LogManager.getLogger(TaskService.class);
    private static final TaskRepository taskRepository = TaskRepository.getInstance();
    private static TaskService instance = null;

    private TaskService() {
    }

    /**
     * Создает и возвращает экземпляр класса
     *
     * @return экземпляр класса {@link TaskService}
     */
    public static TaskService getInstance() {
        synchronized (TaskService.class) {
            return instance == null
                    ? instance = new TaskService()
                    : instance;
        }
    }

    /**
     * Создание задачи и добавление ее в список задач
     *
     * @param name имя задачи
     * @return объект типа {@link Task}
     */
    public Task create(final String name) {
        if (name == null || name.isEmpty())
            return null;
        logger.trace("Создание задачи с именем: " + name);
        return taskRepository.create(name);
    }

    /**
     * Очистка списка задач
     */
    public void clear() {
        taskRepository.clear();
    }

    /**
     * Возвращает список задач
     *
     * @return список задач {@link List}
     */
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    /**
     * Поиск задачи по индексу
     *
     * @param index индекс задачи
     * @return задача {@link Task}
     */
    public Task findByIndex(final int index) throws TaskNotFoundException {
        if (index < 0) return null;
        Task task = taskRepository.findByIndex(index);
        if (task == null){
            logger.error("Задача по данному индексу не найдена!");
            throw new TaskNotFoundException("Задача по данному индексу не найдена!");
        }
        return task;
    }

    /**
     * Поиск задачи по наименованию
     *
     * @param name наименование задачи
     * @return задача {@link Task}
     */
    public Task findByName(final String name) {
        if (name == null || name.isEmpty())
            return null;
        logger.trace("Поиск задачи с именем: " + name);
        return taskRepository.findByName(name);
    }

    /**
     * Поиск задачи по коду
     *
     * @param id колд задачи
     * @return задача {@link Task}
     */
    public Task findById(final Long id) {
        if (id == null) return null;
        logger.trace("Поиск задачи с кодом " + id);
        return taskRepository.findById(id);
    }

    /**
     * Удаление задачи по коду
     *
     * @param id код задачи
     * @return удаленная задача {@link Task}
     */
    public Task removeById(final Long id) {
        if (id == null) return null;
        return taskRepository.removeById(id);
    }

    /**
     * Удаление задачи по наименованию
     *
     * @param name наименование задачи
     * @return удаленная задача {@link Task}
     */
    public Task removeByName(final String name) {
        if (name == null || name.isEmpty())
            return null;
        return taskRepository.removeByName(name);
    }

    /**
     * Удаление задачи по индексу
     *
     * @param index индекс задачи
     * @return удаленная задача {@link Task}
     */
    public Task removeByIndex(final Integer index) {
        if (index < 0 || index > taskRepository.size() - 1) return null;
        return taskRepository.removeByIndex(index);
    }

    /**
     * Изменение задачи
     *
     * @param id          код задачи
     * @param name        наименование задачи
     * @param description описание задачи
     * @return измененная задача {@link Task}
     */
    public Task update(final Long id, final String name, final String description) {
        if (id == null || name == null || name.isEmpty() || description == null)
            return null;
        return taskRepository.update(id, name, description);
    }

    /**
     * Возвращает размер коллекции
     *
     * @return число, определяющее размер коллекции
     */
    public int size() {
        return taskRepository.size();
    }

    /**
     * Список задач, привязанных к проекту
     *
     * @param projectId код проекта
     * @return список типа {@link List}
     * @see Task
     */
    public List<Task> findAllByProjectId(final Long projectId) {
        if (projectId == null) return Collections.emptyList();
        return taskRepository.findAllByProjectId(projectId);
    }

    /**
     * Список задач, непривязанных к проектам
     *
     * @return список типа {@link List}
     * @see Task
     */
    public List<Task> findAllWithoutProject() {
        return taskRepository.findAllWithoutProject();
    }

    /**
     * Удаление задачи из проекта
     *
     * @param taskId код задачи
     * @return задача, удаленная из проекта
     * @see Task
     */
    public Task removeTaskFromProject(final Long taskId) {
        if (taskId == null) return null;
        return taskRepository.removeTaskFromProject(taskId);
    }

}
