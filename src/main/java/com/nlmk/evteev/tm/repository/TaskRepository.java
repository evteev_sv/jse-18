package com.nlmk.evteev.tm.repository;

import com.nlmk.evteev.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Класс для работы с объектом задачи {@link Task}
 */
public class TaskRepository {

    private static TaskRepository instance = null;
    private final List<Task> tasks = new ArrayList<>();

    private TaskRepository() {
    }

    /**
     * Возвращает экземпляр репозитория задач
     *
     * @return репозиторий задач {@link TaskRepository}
     */
    public static TaskRepository getInstance() {
        synchronized (TaskRepository.class) {
            return instance == null
                    ? instance = new TaskRepository()
                    : instance;
        }
    }

    /**
     * Создание задачи и добавление ее в список задач
     *
     * @param name имя задачи
     * @return объект типа {@link Task}
     */
    public Task create(final String name) {
        final Task task = new Task(name);
        tasks.add(task);
        return task;
    }

    /**
     * Очистка списка задач
     */
    public void clear() {
        tasks.clear();
    }

    /**
     * Возвращает список задач
     *
     * @return список задач {@link java.util.List}
     */
    public List<Task> findAll() {
        return tasks;
    }

    /**
     * Поиск задачи по индексу
     *
     * @param index индекс задачи
     * @return задача {@link com.nlmk.evteev.tm.entity.Task}
     */
    public Task findByIndex(final int index) {
        if (index < 0 || index > tasks.size())
            return null;
        return tasks.get(index);
    }

    /**
     * Поиск задачи по наименованию
     *
     * @param name наименование задачи
     * @return задача {@link Task}
     */
    public Task findByName(final String name) {
        for (Task task : tasks) {
            if (task.getName().equals(name))
                return task;
        }
        return null;
    }

    /**
     * Поиск задачи по коду
     *
     * @param id колд задачи
     * @return задача {@link Task}
     */
    public Task findById(final Long id) {
        for (Task task : tasks) {
            if (task.getId().equals(id))
                return task;
        }
        return null;
    }

    /**
     * Удаление задачи по коду
     *
     * @param id код задачи
     * @return удаленная задача {@link Task}
     */
    public Task removeById(final Long id) {
        final Task task = findById(id);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    /**
     * Удаление задачи по наименованию
     *
     * @param name наименование задачи
     * @return удаленная задача {@link Task}
     */
    public Task removeByName(final String name) {
        final Task task = findByName(name);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    /**
     * Удаление задачи по индексу
     *
     * @param index индекс задачи
     * @return удаленная задача {@link Task}
     */
    public Task removeByIndex(final Integer index) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    /**
     * Изменение задачи
     *
     * @param id          код задачи
     * @param name        наименование задачи
     * @param description описание задачи
     * @return измененная задача {@link Task}
     */
    public Task update(final Long id, final String name, final String description) {
        Task task = findById(id);
        if (task == null)
            return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    /**
     * Возвращает размер коллекции
     *
     * @return число, определяющее размер коллекции
     */
    public int size() {
        return tasks.size();
    }

    /**
     * Список задач, привязанных к проекту
     *
     * @param projectId код проекта
     * @return список типа {@link List}
     * @see Task
     */
    public List<Task> findAllByProjectId(final Long projectId) {
        return tasks.stream().filter(task -> task.getProjectId().equals(projectId)).collect(Collectors.toList());
    }

    /**
     * Добавление задачи к проекту
     *
     * @param projectId код проекта
     * @param taskId    код задачи
     * @return задача, добавленная к проекту
     */
    public Task addTaskToProjectByIds(final Long projectId, final Long taskId) {
        final Task task = findById(taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    /**
     * Список задач, непривязанных к проектам
     *
     * @return список типа {@link List}
     * @see Task
     */
    public List<Task> findAllWithoutProject() {
        return tasks.stream().filter(task -> task.getProjectId() == null).collect(Collectors.toList());
    }

    /**
     * Удаление задачи из проекта
     *
     * @param taskId код задачи
     * @return задача, удаленная из проекта
     * @see Task
     */
    public Task removeTaskFromProject(final Long taskId) {
        final Task task = findById(taskId);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

}
