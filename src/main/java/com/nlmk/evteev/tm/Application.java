package com.nlmk.evteev.tm;

import com.nlmk.evteev.tm.controller.ProjectTaskController;
import com.nlmk.evteev.tm.controller.SystemController;
import com.nlmk.evteev.tm.controller.UserController;
import com.nlmk.evteev.tm.enumerations.UserRole;
import com.nlmk.evteev.tm.exceptions.ProjectNotFoundException;
import com.nlmk.evteev.tm.exceptions.TaskNotFoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

import static com.nlmk.evteev.tm.constant.TerminalConst.*;

/**
 * Основной класс
 */
public class Application {

    private final static Logger logger = LogManager.getLogger(Application.class);

    private final SystemController systemController = SystemController.getInstance();
    private final ProjectTaskController projectTaskController = new ProjectTaskController();

    {
        UserController.getInstance().createUser("ivanov", "qwerty", UserRole.ADMIN,
                "Иванов", "Иван", "Иванович");
        UserController.getInstance().createUser("petrov", "asdzxc", UserRole.USER,
                "Петров", "Петр", "Петрович");
        projectTaskController.getProjectController().createProject("Проект № 1");
        projectTaskController.getProjectController().createProject("Проект № 2");
    }

    /**
     * Точка входа
     *
     * @param args дополнительные аргументы запуска
     */
    public static void main(String[] args) {
        final Scanner scanner = new Scanner(System.in);
        final Application application = new Application();
        UserController.getInstance().userLogin();
        application.systemController.displayWelcome(UserController.getInstance().getAppUser());
        try {
            application.run(args);
        } catch (TaskNotFoundException | ProjectNotFoundException e) {
            logger.error(e);
        }
        String command = "";

        while (!CMD_EXIT.equals(command)) {
            command = scanner.nextLine();
            try {
                application.run(command);
            } catch (ProjectNotFoundException | TaskNotFoundException e) {
                logger.error(e);
            }
            System.out.println();
        }
    }

    /**
     * Основной метод исполнения
     *
     * @param args дополнительные параметры запуска
     */
    public void run(final String[] args) throws TaskNotFoundException, ProjectNotFoundException {
        if (args == null || args.length < 1) return;
        final String param = args[0];
        run(param);
    }

    /**
     * Обработка консольного ввода
     *
     * @param cmdString команда с консоли
     */
    public void run(final String cmdString) throws ProjectNotFoundException, TaskNotFoundException {
        if (cmdString == null || cmdString.isEmpty()) return;
        systemController.addToHistory(cmdString);
        if (UserController.getInstance().getAppUser() == null)
            if (!cmdString.equals(USER_LOGIN) && !cmdString.equals(CMD_EXIT)) {
                System.out.println("Выполнение команды прервано: необходимо войти в систему.");
                return;
            }
        switch (cmdString) {
            case CMD_VERSION:
                systemController.displayVersion();
                return;
            case CMD_HELP:
                systemController.displayHelp();
                return;
            case CMD_ABOUT:
                systemController.displayAbout();
                return;
            case CMD_EXIT:
                systemController.displayExit();
            case VIEW_HISTORY:
                systemController.displayHistory();
                return;

            case PROJECT_CREATE:
                projectTaskController.getProjectController().createProject();
                return;
            case PROJECT_CLEAR:
                projectTaskController.getProjectController().clearProject();
                return;
            case PROJECT_LIST:
                projectTaskController.getProjectController().listProject();
                return;
            case PROJECT_VIEW:
                projectTaskController.getProjectController().viewProjectByIndex();
                return;
            case PROJECT_VIEW_WITH_TASKS:
                projectTaskController.viewProjectWithTasks();
                return;
            case PROJECT_REMOVE_BY_ID:
                projectTaskController.getProjectController().removeProjectById();
                return;
            case PROJECT_REMOVE_BY_NAME:
                projectTaskController.getProjectController().removeProjectByName();
                return;
            case PROJECT_REMOVE_BY_INDEX:
                projectTaskController.getProjectController().removeProjectByIndex();
                return;
            case PROJECT_UPDATE_BY_INDEX:
                projectTaskController.getProjectController().updateProjectByIndex();
                return;
            case PROJECT_UPDATE_BY_ID:
                projectTaskController.getProjectController().updateProjectById();
                return;
            case PROJECT_ADD_TASK_BY_IDS:
                projectTaskController.addTaskToProjectByIds();
                return;
            case PROJECT_REMOVE_TASK_BY_IDS:
                projectTaskController.removeTaskFromProjectById();
                return;
            case PROJECT_REMOVE_TASKS:
                projectTaskController.clearProjectTasks();
                return;
            case PROJECT_REMOVE_WITH_TASKS:
                projectTaskController.removeProjectWithTasks();
                return;
            case PROJECT_CHANGE_OWNER:
                projectTaskController.getProjectController().assignProjectToUser();
                return;

            case TASK_CREATE:
                projectTaskController.getTaskController().createTask();
                return;
            case TASK_CLEAR:
                projectTaskController.getTaskController().clearTask();
                return;
            case TASK_LIST:
                projectTaskController.getTaskController().listTask();
                return;
            case TASK_VIEW:
                projectTaskController.getTaskController().viewTaskByIndex();
                return;
            case TASK_VIEW_WITHOUT_PROJECT:
                projectTaskController.getTaskController().findAllWithoutProject();
                return;
            case TASK_VIEW_BY_PROJECT:
                projectTaskController.getTaskController().findAllByProjectId();
                return;
            case TASK_REMOVE_BY_ID:
                projectTaskController.getTaskController().removeTaskById();
                return;
            case TASK_REMOVE_BY_NAME:
                projectTaskController.getTaskController().removeTaskByName();
                return;
            case TASK_REMOVE_BY_INDEX:
                projectTaskController.getTaskController().removeTaskByIndex();
                return;
            case TASK_UPDATE_BY_ID:
                projectTaskController.getTaskController().updateTaskById();
                return;
            case TASK_UPDATE_BY_INDEX:
                projectTaskController.getTaskController().updateTaskByIndex();
                return;

            case USER_CREATE:
                UserController.getInstance().createUser();
                return;
            case USER_UPDATE:
                UserController.getInstance().updateUser();
                return;
            case USER_DELETE:
                UserController.getInstance().deleteUser();
                return;
            case USER_LIST:
                UserController.getInstance().listUsers();
                return;
            case USER_LOGIN:
                UserController.getInstance().userLogin();
                return;
            case USER_VIEW:
                UserController.getInstance().viewUserProfile();
                return;
            case USER_CHANGE_PASSWORD:
                UserController.getInstance().changeUserPassword();
                return;
            case USER_PROFILE_EDIT:
                UserController.getInstance().editUserProfile();
                return;
            case USER_LOGOUT:
                UserController.getInstance().userLogOut();
                return;

            default:
                systemController.displayError();
        }
    }

}
